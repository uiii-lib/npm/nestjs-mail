import aws from 'aws-sdk';

import { ConfigService } from '@uiii-lib/nestjs-config';

import { MailConfig } from '../src/mail.config';
import { MailService } from '../src/mail.service';

jest.mock('aws-sdk', () => {
	const sendRawEmail = jest.fn().mockReturnValue({
		promise: jest.fn().mockReturnValue(Promise.resolve(true))
	});

	return {
		SES: function SES(this: any) {
			this.sendRawEmail = sendRawEmail;
		}
	}
});

jest.mock('@uiii-lib/nestjs-config');

describe("Mail service", () => {
	let mailService: MailService;

	beforeEach(() => {
		process.env.MAIL_FROM = "from@example.com";
		const configService = new ConfigService();

		const mailConfig = new MailConfig(configService);
		mailService = new MailService(mailConfig);
	})

	describe("send", () => {
		it("should send mail via AWS SES", async () => {
			const to = "test@example.com";
			const subject = "Message subject";
			const message = "Message body";

			await mailService.send(to, subject, message);

			expect(new aws.SES().sendRawEmail).toHaveBeenCalled();
		});
	});
});
