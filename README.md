# Mail module for NestJS

Nejst module for sending e-mails. Uses AWS SES.

## Usage (server)

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install the package

> Requires `aws-sdk` and `@uiii-lib/nestjs-config` as peer dependency

```
npm install --save @uiii-lib/nestjs-mail aws-sdk @uiii-lib/nestjs-config
```

Set `MAIL_FROM` environment variable to e-mail address you want to set as FROM header of sent e-mails.

Add mail module to `imports` of a module which you want to use it in.

```ts
import { MailModule } from '@uiii-lib/nestjs-mail';

@Module({
	imports: [
		MailModule
	],
	...
})
export class SomeModule
```

Send mail

```ts
import { Injectable } from '@nestjs/common';

import { MailService } from '@uiii-lib/nestjs-mail';

@Injectable()
export class SomeModuleService {
	constructor(private mailService: MailService) {}

	async doAction(): string {
		...
		await this.mailService.send("to@mail.com", "subject", "message");
	}
}
```
