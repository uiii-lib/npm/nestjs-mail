import Nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import aws from 'aws-sdk';

import { Injectable } from '@nestjs/common';

import { MailConfig } from './mail.config';

@Injectable()
export class MailService {
	protected mailer: Mail;

	constructor(private config: MailConfig) {
		this.mailer = Nodemailer.createTransport({
			SES: new aws.SES({
				apiVersion: '2010-12-01'
			})
		});
	}

	async send(to: string, subject: string, message: string) {
		await this.mailer.sendMail({
			from: this.config.mailFrom,
			to,
			subject,
			text: message
		});
	}
}
