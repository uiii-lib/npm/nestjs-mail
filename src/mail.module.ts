import { Module } from '@nestjs/common';
import { ConfigModule } from '@uiii-lib/nestjs-config';

import { MailService } from './mail.service';
import { MailConfig } from './mail.config';

@Module({
	imports: [
		ConfigModule
	],
	providers: [
		MailConfig,
		MailService
	],
	exports: [
		MailService
	]
})
export class MailModule {}
