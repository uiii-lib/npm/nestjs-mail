import { Injectable } from '@nestjs/common';
import { ConfigService } from '@uiii-lib/nestjs-config';

@Injectable()
export class MailConfig {
	constructor(private config: ConfigService) {}

	get mailFrom(): string {
		return this.config.get('MAIL_FROM', true)!;
	}
}
